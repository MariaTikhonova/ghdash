# FLOW 

Resorce Profile accepts github username from user via form at url http://#{host}/profiles/new.
Service object ProfileGetter at app/services/ accepts username within profile as its only parameter (validated for presence) when created, and  triggers Client of Octokit lib from adapters/adapter.rb. Adapter, after setting client with auth_token for app to work with api ( secret stored at .env), provides method consisting octokit's method for retrieving github's user by username with all its fields, its resource. Than service object calls adapter's instance with this method in instance method passed to another controller's, profile_infos, #index, which holds app's root url and shows users' with valid github usernames profiles with maximum info's as jsons. 

# SCREENSHOTS 

![screenshot](app/assets/images/1.png)

# YOUR TASK

Завдання - просто rails апка, що на головну сторінку виводить максимум інформації про мій профіль з github. В апці має бути ServiceObject що відповідає за отримання даних від Adapter-а, що у свою чергу вже комунікує з Github API
Всі обєкти повинні бути покриті тестами. HTTP-коли з використанням VCR