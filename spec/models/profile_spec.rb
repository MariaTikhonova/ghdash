require 'rails_helper'

RSpec.describe Profile, type: :model do

	 let(:profile) { FactoryBot.create(:profile) }

  it 'should return valid profile' do
    expect(profile).to be_valid
  end
  describe 'valid?' do
    it 'should not save profile without username' do
      profile.username = ''
      expect(profile).to be_invalid
    end
  end
end
