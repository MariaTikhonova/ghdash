require 'rails_helper'

RSpec.describe ProfileGetter do
  
  subject { ProfileGetter.new(profile) }

  let(:profile) { FactoryBot.create(:profile) }
  let(:adapter) { Adapter::GitHub.new }

  describe "passing valid username", :vcr do

    it "is successful" do
      expect(subject.call).to be_success
    end
    
    it "invokes user_profile" do
      user_profile = subject.call.user_profile
      expect(user_profile).to be_persisted
      expect(profile.username).to eq("rghostme")
    end
  end
  
  describe "doesn't connect to adapter", :vcr do
    let(:client) { instance_double(access_token: 
        nil) }
    
    it "is unsuccessful" do
      expect(subject.call).not_to be_success
      expect(subject.error_message).to match("can't connect to service")
    end
  end
end