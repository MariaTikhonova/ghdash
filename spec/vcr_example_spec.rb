require 'spec_helper'

describe "VCR-RSpec integration" do
  
  def make_http_request
    Faraday.get('localhost', '/', 3000).body
  end

  context "without an explicit cassette name", :vcr do
    it 'records an http request' do
      make_http_request.should == 'success'
    end
  end

  context "with an explicit cassette name", :vcr do
    "faraday_example"

    it 'records an http request' do
      make_http_request.should == 'success'
    end
  end
end