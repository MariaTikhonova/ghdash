require 'rails_helper'

RSpec.describe ProfileInfosController, type: :controller do
	 let(:profile) { FactoryBot.create(:profile) }
  let(:profile_info) { FactoryBot.create(:profile_info) }
  let(:profile_getter) { ProfileGetter.new(profile) }

	 describe "GET#index" do
	   context "all profiles", :vcr do
  	 	 let(:profile) { FactoryBot.create(:profile) }
      let(:profile_getter) { ProfileGetter.new(profile) }

  	 	 it 'should return profile infos' do
        get :index
        profiles = assigns(:profiles)
        before :each do
      	   profile_getter.call
        end
        expect(profile_infos.length).to eq(1)
        expect(profile_infos[0]).to eq(profile)
      end
	   end
	 end
end


