require 'rails_helper'

RSpec.describe ProfilesController, type: :controller do

	 let(:profile) { FactoryBot.create(:profile) }

	 describe "POST#create" do
	   context "new profile", :vcr do
      it 'adds a new profile' do
      	 before do
      	 	 post :create, username: "rghostme"
      	 end
        expect(response.status).to eq(200)
        expect(Profile.count).to eq(1)
      end
    end
  end
end
