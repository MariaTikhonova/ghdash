Rails.application.routes.draw do
  resources :profile_infos, only: :index,  defaults: { format: :json }
  resources :profiles, only: [:create, :new]
  root 'profile_infos#index'
end
