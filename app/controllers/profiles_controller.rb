class ProfilesController < ApplicationController
  
  def new
  	 @profile = Profile.new
  end

	 def create 
    @profile = Profile.new(profile_params)
    if @profile.save
    	username = @profile.username
      redirect_to root_url
    else
      
      redirect_back(fallback_location: root_path, notice: "Username has to be provided")
    end
  end

  def profile_params
  	 params.require(:profile).permit(:username)
  end
end
