class ProfileInfosController < ApplicationController

   def index
  	 @profiles = Profile.all
     @profile_infos = @profiles.map do |profile|
        ProfileGetter.new(profile).user_profile
      end
     render json: @profile_infos
   end
end
