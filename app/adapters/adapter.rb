module Adapter
  class GitHub

    def initialize
      @client ||=  Octokit::Client.new(access_token: 
        ENV["OAUTH_TOKEN"])
    end

    def profile_for_username(username)
    	 @client.user(username)
    end
  end
end