   class ProfileGetter

    def initialize(profile)
    	 @profile = profile
    	 @adapter = Adapter::GitHub.new
    end

    def user_profile
      @adapter.profile_for_username(@profile.username)
    end
  end